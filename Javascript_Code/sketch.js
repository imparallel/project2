/*
	Project 2
	Name of Project: ABCDE!! (Avoid Bullets & Crash Da Enemy !!)
	Author: 20150622 Changbok Lee
	Date: 2020.06
*/




let gamestate = 2;
let px = 0;
let py = 0;
let Exp = 0;
let level = 1;
let needExp = 2 ** (level-1);
let cr = 0;
let cg = 0;
let cb = 0;
let cc = 1;
let conenection = 0;

function preload() {
	bg = loadImage ('data/bg.png');
	nowplaying = loadImage ('data/nowplaying.png');
	loading = loadImage ('data/loading.png');
	start = loadImage ('data/start.png');
	ximage = loadImage ('data/x.png');
	connectionclosed = loadImage ('data/connectionclosed.png');
	clicksound = loadSound('data/clicksound.mp3');
}

function setup (){
	connect();
	  createCanvas(800, 600);
	  clicksound.setVolume(0.1);
	  clicksound.play();
	if (conenection == 1)
	{
		ws.send(`{"px":"${px}"}`);
		ws.send(`{"py":"${py}"}`);
  		ws.send(`{"cr":"${cr}"}`);
		ws.send(`{"cg":"${cg}"}`);
		ws.send(`{"cb":"${cb}"}`);
	}
	
}

function draw(){
  background(120);
  if (gamestate == 0)
  {
	image (nowplaying, 0, 0, width, height)
  }
  else if (gamestate == 1)
  {
	image (bg, 0, 0, width, height)
	fill(cr,cg,cb,100);
    noStroke();
    ellipseMode(CENTER);
	ellipse(width/2, height/2, 240+px, 240+py);
	fill(0,20);
	noStroke();
	rectMode(CENTER)
	//rect(width/2,height*4/5,400,10);
	//rect(width/5+5,height/2,10,300);
	fill(0,150);
	noStroke();
	rectMode(CENTER)
	rect(width/2+px*4,height*4/5,15,25); // character width bar
	rect(width/5+5,height/2-py*3,25,15); // character height bar
	fill(0,100);
	noStroke();
	rectMode(CORNER)
	rect(width/2-250,height/6,500,20);
	fill(255,255,0,180);
	stroke(255);
	rectMode(CORNER)
	for(let i=0;i<Exp;i++)
	{
		rect(width/2-250+500*(i/needExp),height/6,500*(1/needExp),20);
	}
	fill(0);
	stroke(0);
	textAlign(CENTER);
	textSize(24);
	text('lv.'+ level, width/2, height/6+60);

	noStroke();
	rectMode(CENTER)
	fill(100,100,100,200);
	rect(width*4/5-15,height/2+135,40,40);
	fill(61,111,255,200);
	rect(width*4/5-15,height/2+90,40,40);
	fill(255,134,61,200);
	rect(width*4/5-15,height/2+45,40,40);
	fill(255,61,125,200);
	rect(width*4/5-15,height/2,40,40);
	fill(16,205,83,200);
	rect(width*4/5-15,height/2-45,40,40);
	fill(255,61,61,200);
	rect(width*4/5-15,height/2-90,40,40);
	fill(250,250,50,200);
	rect(width*4/5-15,height/2-135,40,40);
	//image (ximage, 0,0, 40,40);
	if (level < 7)
	{
		image (ximage, width*4/5-35, height/2-155, 40,40);
		if (level < 6)
		{
			image (ximage, width*4/5-35, height/2-110, 40,40);
			if (level < 5)
			{
				image (ximage, width*4/5-35, height/2-65, 40,40);
				if (level < 4)
				{
					image (ximage, width*4/5-35, height/2-20, 40,40);
					if (level < 3)
					{
						image (ximage, width*4/5-35, height/2+25, 40,40);
						if (level < 2)
						{
							image (ximage, width*4/5-35, height/2+70, 40,40);
						}
					}
				}
			}
		}
	}
	fill(0,0);
	stroke(255,0,0,255)
	rect(width*4/5-15,height/2+180-45*cc,40,40);
	
  }
  else if (gamestate == 2)
  {
	image (loading, 0, 0, width, height)
  }
  else if (gamestate == 3)
  {
	image (start, 0, 0, width, height)
  }
  else if (gamestate == 4)
  {
	image (connectionclosed, 0, 0, width, height)
  }
  
}

function mouseDragged() // Drag two bars to change character's appearance
{
	if (gamestate == 1)
	{
		if ((220 < mouseX && mouseX < 580) && (450 < mouseY && mouseY < 510))
		{
			px = (mouseX-400)/4;
			ws.send(`{"px":"${px}"}`);
		}
		if ((135 < mouseX && mouseX < 195) && (165 < mouseY && mouseY < 435))
		{
			py = -(mouseY-300)/3;
			ws.send(`{"py":"${py}"}`);
		}
	}
}

function mousePressed() 
{
	if (gamestate == 4)
	{
		connect();
	}
	if (gamestate == 0)
	{
		clicksound.setVolume(0.1);
		clicksound.play()
		ws.send(`{"Pause":"1"}`);
		gamestate = 1;
	}
	if (gamestate == 1)
	{
		if ((220 < mouseX && mouseX < 580) && (450 < mouseY && mouseY < 510))
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			px = (mouseX-400)/4;
			ws.send(`{"px":"${px}"}`);
		}

		if ((135 < mouseX && mouseX < 195) && (165 < mouseY && mouseY < 435))
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			py = -(mouseY-300)/3;
			ws.send(`{"py":"${py}"}`);
		}

		if ((605 < mouseX && mouseX < 645) && (415 < mouseY && mouseY < 455) && level > 0)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 1;
			cr=0;
			cg=0;
			cb=0;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (370 < mouseY && mouseY < 410) && level > 1)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 2;
			cr=61;
			cg=111;
			cb=255;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (325 < mouseY && mouseY < 365) && level > 2)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 3;
			cr=255;
			cg=134;
			cb=61;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (280 < mouseY && mouseY < 320) && level > 3)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 4;
			cr=255;
			cg=61;
			cb=125;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (235 < mouseY && mouseY < 275) && level > 4)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 5;
			cr=16;
			cg=205;
			cb=83;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (190 < mouseY && mouseY < 230) && level > 5)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 6;
			cr=255;
			cg=61;
			cb=61;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
		else if ((605 < mouseX && mouseX < 645) && (145 < mouseY && mouseY < 185) && level > 6)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 7;
			cr=250;
			cg=250;
			cb=50;
			ws.send(`{"cr":"${cr}"}`);
			ws.send(`{"cg":"${cg}"}`);
			ws.send(`{"cb":"${cb}"}`);
		}
	}
	
	//ws.send(`{"mouseX":"${mouseX}"}`);
	//ws.send(`{"mouseY":"${mouseY}"}`);
}
  
  function keyPressed()
{
	if (key=='a' || key=='A' && gamestate == 1)
	{
		if (px >= -40)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			px -= 5;
		}
		else if (px > -45)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			px = -45;
		}
		ws.send(`{"px":"${px}"}`);
	} 
	else if (key=='d' || key=='D' && gamestate == 1)
	{
		if (px <= 40)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			px += 5;
		}
		else if (px < 45)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			px = 45;
		}
		ws.send(`{"px":"${px}"}`);
	}
	else if (key=='w' || key=='W' && gamestate == 1)
	{
		if (py <= 40)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			py += 5;
		}
		else if (py < 45)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			py = 45;
		}
		ws.send(`{"py":"${py}"}`);
	}
	else if (key=='s' || key=='S' && gamestate == 1)
	{
		if (py >= -40)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			py -= 5;
		}
		else if (py > -45)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			py = -45;
		}
		ws.send(`{"py":"${py}"}`);
	}
	else if (key=='c' || key=='C' && gamestate == 1)
	{
		if (cc < level)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc += 1;
		}
		else if (cc == level)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cc = 1;
		}
		if (cc == 1)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=0;
			cg=0;
			cb=0;
		}
		else if (cc == 2)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=61;
			cg=111;
			cb=255;
		}
		else if (cc == 3)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=255;
			cg=134;
			cb=61;
		}
		else if (cc == 4)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=255;
			cg=61;
			cb=125;
		}
		else if (cc == 5)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=16;
			cg=205;
			cb=83;
		}
		else if (cc == 6)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=255;
			cg=61;
			cb=61;
		}
		else if (cc == 7)
		{
			clicksound.setVolume(0.1);
			clicksound.play()
			cr=250;
			cg=250;
			cb=50;
		}
		ws.send(`{"cr":"${cr}"}`);
		ws.send(`{"cg":"${cg}"}`);
		ws.send(`{"cb":"${cb}"}`);
	}
}

  // called when loading the page
  function connect() {
	ws = new WebSocket("ws://localhost:8025/test");
  
	ws.onopen = function () {
	  // Web Socket is connected, send data using send()
	  clicksound.setVolume(0.1);
	  clicksound.play()
	  conenection = 1;
	  gamestate = 3;
	  console.log("Ready...");
	};
  
	ws.onmessage = function (evt) {
	  var received_msg = evt.data;
	  console.log("Message is received..." + received_msg);
	  if (received_msg == "gamestart") 
	  {
		  gamestate = 0;
	  }
	  else if (received_msg == "gameover")
	  {
		  gamestate = 1;
	  }
	  else if (received_msg == "resume")
	  {
		clicksound.setVolume(0.1);
		clicksound.play();
		  gamestate = 0;
	  }
	  else if (received_msg == "nowloading")
	  {
		  gamestate = 2;
	  }
	  else if (received_msg == "exp")
	  {
		Exp += 1;
		if (Exp == 2 ** (level-1))
		{
			Exp = 0;
			level += 1;
			needExp = 2 ** (level-1);
		}
		console.log(Exp);
		console.log(level);
	  }

	};
  
	ws.onclose = function () {
	  // websocket is closed.
	  gamestate = 4;
	  conenection = 0;
	  console.log("Connection is closed...");
	};

  };