# Project 2 template #

This is the template for your project 2 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1jmFpRdEZJ_Cmk9Hp1q_I7Co9vRo-d5a3fUHcKBYSAPk/edit?usp=sharing).

## Personal information to add

* **Name**: 
* **Student ID**: 
* **email**: 

## Important notice before submitting

Make sure to create a branch with your studentID number before

---

## Table of content

* **Java_Code** source code (from Project 1): you can modify it
* **Javascript_Code** source code
* Demo **Video** (the actual video, not the link)
* **Description** of projects and notes in README.md (this file). 

## Description of the project

Please modify this file to include a description of your project.

You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you
* Anything else you want to tell me